// js/script.js

// Opening script for the whole app

const foodContent = document.getElementById("foodSelectorContent");
const checkMealButton = document.getElementById("infoDisplayDoneBtn");

const jsonData = new JsonManager('food.json');
const infoDisplayer = new InfoDisplayer();

checkMealButton.addEventListener("click", () => {
	endApp(infoDisplayer.totalGL);
});

function startApp(data) { // Generate the list and the events

	for (i=0; i<data.length; i++) {

		let currDataNode = document.createElement("DIV");
		let currDataP = document.createElement("P");

		currDataP.appendChild(document.createTextNode(data[i].title + " (~" + data[i].g + "g)"));
		currDataNode.appendChild(currDataP);

		foodContent.appendChild(currDataNode);
		// at this point the whole list is created

		currDataNode.classList.add("foodItem");
		currDataNode.id = i + "|" + data[i].title + "|" + data[i].GL;

		currDataNode.addEventListener("click", function() {
			currDataNode.classList.toggle("foodSelected");
			let currDataInfos = currDataNode.id.split("|");
			infoDisplayer.updateInfos(currDataInfos);
		});
	}

}

function endApp(totalGL) { // "check your meal" button
	const endConclusion = document.getElementById("endAppConclusion");

	if (totalGL < 5) {
		endConclusion.textContent = "This wasn't even a meal, you troll";
	}
	else if (totalGL < 10) {
		endConclusion.textContent = "Are you actually alive with this kind of meal ?";
	}
	else if (totalGL < 20) {
		endConclusion.textContent = "Perfect, you are eating as you should !";
	}
	else if (totalGL < 30) {
		endConclusion.textContent = "Warning ! You seem to be eating too much candy.";
	}
	else {
		endConclusion.textContent = "By my calculations, you are currently dead.";
	}


	document.getElementById("endAppScreen").style.display = "flex";
	document.getElementById("appContainer").style.display = "none";
	document.getElementById("endAppTotalGL").textContent = totalGL;
}