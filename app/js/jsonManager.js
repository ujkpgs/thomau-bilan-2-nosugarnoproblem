// jsonManager.js

// Reads food.json and creates a JsonManager object to manipulate the informations

class JsonManager {
	constructor(src) {
		this.src = src;
		this.getDatas(this.src).then((resp) => {
			this.datas = resp;
			startApp(this.datas);
		});
	}

	async getDatas(src) {
		let answer = await fetch(src);
		return answer.json();
	}
}