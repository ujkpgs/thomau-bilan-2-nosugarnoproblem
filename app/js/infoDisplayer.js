// this object is here to display informations on the bottom of the page

class InfoDisplayer {
	constructor() {
		this.foodList = [];

		this.totalGL = 0;
		this.totalFood = 0;

		this.totalFoodDiv = document.getElementById("infoDisplayTotalFood");
		this.totalGLDiv = document.getElementById("infoDisplayTotalGL");
		
		this.updateInfos();
	}

	updateInfos(clickedID = null) {

		this.foodList = document.getElementsByClassName("foodSelected");

		this.totalFood = this.foodList.length;

		if (clickedID) {
			this.totalGL = 0;
			for (let i=0; i<this.foodList.length; i++) {
				let tempIdData = this.foodList[i].id.split("|");
				this.totalGL += Number(tempIdData[2]);
			}
		}

		
		this.totalFoodDiv.textContent = this.totalFood;
		this.totalGLDiv.textContent = this.totalGL;
	}
}