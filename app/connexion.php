<?php
require 'php/connectDtb.php';

if (isset($_SESSION['loggedAs'])) { // if already logged in, redirect to app
	header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Login | No Sugar, No Problem</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php

	if (isset($_GET['source'])) {
		switch ($_GET['source']) {
			case 'noMatch':
			echo '<div class="connectAlert" id="noMatch"><p>There is no account existing with this username. Do you wish to <a href="php/createUser.php">create one</a> ?</p></div>';
			break;
			case 'noRoute':
			echo '<div class="connectAlert" id="noRoute"><p>Please login before using this service.</p></div>';
			break;
			case 'wrongPass':
			echo '<div class="connectAlert" id="wrongPass"><p>Incorrect password.</p></div>';
		}
	}
	?>

	<div id="loginForm">

		<h1>Login</h1>
		<div class="underTitleBlueBar"></div>

		<form method="POST" action="php/connexionProcess.php">

			<input type="hidden" name="source" value="connexion">

			<div class="loginFormGroup">
				<label for="usernameConnexion">Username</label>
				<input required placeholder="Your username..." type="username" name="username" id="usernameConnexion">
			</div>
			<div class="loginFormGroup">
				<label for="passwordConnexion">Password</label>
				<input required placeholder="Your password..." type="password" name="password" id="passwordConnexion">
			</div>
			<div class="loginFormGroup">
				<button name="submit" type="submit">Login !</button>
			</div>

		</form>
	</div>
	<div class="formLinks" id="loginLink">
		<p>Still not a member ?<br><a href="php/createUser.php">Create an account</a> !</p>
	</div>

	<!-- SCRIPTS -->
	<script type="text/javascript" src="js/alertCloser.js"></script>
</body>
</html>