<?php
require 'php/connectDtb.php';

if (!isset($_SESSION['loggedAs'])) { // if is not connected, connect
	header('Location: connexion.php?source=noRoute');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Online Glycemic Load Calculator | No Sugar, No Problem</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<?php
	if (isset($_SESSION['loggedAs'])) {
		echo '<div class="loggedInfo"><p class="loggedFirstLine">Connected as : </p><p class="loggedInfoUsername">'.$_SESSION['loggedAs'].'</p><p class="logoutLink"><a href="disconnect.php">Logout...</a></p></div>';
	}
	?>
	<div id="appContainer">
		<div id="foodSelectorContainer">
			<div id="foodSelectorTitle">
				<h1>Select your Food</h1>
				<div class="underTitleBlueBar"></div>
			</div>
			<div id="foodSelectorContent">
			</div>
		</div>

		<div id="infoDisplayContainer">
			<p>Total Food Selected : <span id="infoDisplayTotalFood"></span></p>
			<p>Total GL : <span id="infoDisplayTotalGL"></span></p>
			<div id="infoDisplayDoneBtn">Check your meal !</div>
		</div>
	</div>

	<div id="endAppScreen">
		<p id="endAppGLindic">Your meal's total GL : <span id="endAppTotalGL">5</span></p>
		<div class="underTitleBlueBar"></div>
		<p id="endAppConclusion">Error</p>
		<a id="endAppSpecialistCall" href="tel:+33632096792">Call a Specialist</a>
	</div>

	<!-- SCRIPTS -->
	<script type="text/javascript" src="js/jsonManager.js"></script>
	<script type="text/javascript" src="js/infoDisplayer.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
</body>
</html>