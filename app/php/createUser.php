<?php
require 'connectDtb.php';

if (isset($_SESSION['loggedAs'])) { // is connected => redirected to app
	header('Location: ../index.php');
}

$verifyUnique = $dtb->prepare('SELECT * FROM users WHERE username=:username');
$createAccount = $dtb->prepare('INSERT INTO users (username, password) VALUES (:username, :password)');

$inputedName = strip_tags($_POST['username']);
$inputedPass = strip_tags($_POST['password']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Create Account | No Sugar, No Problem</title>
	<link rel="stylesheet" type="text/css" href="../css/reset.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<?php

	if (isset($_POST['submit'])) { // if user just initated an account creation
		$verifyUnique->execute(array(
			'username' => $inputedName
		));
	if (!$verifyUnique->fetch()) { // Final process
		$newPassword = password_hash($inputedPass, PASSWORD_DEFAULT);
		$createAccount->execute(array(
			'username' => $inputedName,
			'password' => $newPassword
		));
		echo '<div class="connectAlert"><p>Your account has been successfully created !<br> <a href="../connexion.php">Go to login page</a> !</p></div>';
	}
	else { // if username already taken
		echo '<div class="connectAlert"><p>Your username already exists in the database. Please choose another one or <a href="../connexion.php">login to your account</a>.</p></div>';
	}
}

?>

<div id="createForm">

	<h1>Create Account</h1>
	<div class="underTitleBlueBar"></div>

	<form method="POST">

		<div class="createFormGroup">
			<label for="usernameCreateAcc">Username</label>
			<input required placeholder="Choose a username..." type="username" name="username" id="usernameCreateAcc">
		</div>
		<div class="createFormGroup">
			<label for="passwordCreateAcc">Password</label>
			<input required placeholder="Choose a password..." type="password" name="password" id="passwordCreateAcc">
		</div>

		<div class="createFormGroup">
			<button name="submit" type="submit">Create Account !</button>
		</div>

	</form>
</div>
<div class="formLinks" id="createLink">
	<p>Already registered ? <a href="../connexion.php">Login now</a> !</p>
</div>

<!-- SCRIPTS -->
<script type="text/javascript" src="../js/alertCloser.js"></script>
</body>
</html>