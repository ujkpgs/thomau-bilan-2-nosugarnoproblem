<?php
require 'connectDtb.php';

$inputedName = strip_tags($_POST['username']);
$inputedPass = strip_tags($_POST['password']);

//

$verifyData = $dtb->prepare('SELECT * FROM users WHERE username=:name');
$verifyData->execute(array(
	'name' => $inputedName
));
$userData = $verifyData->fetch();

//

if (!$_POST['source'] && !$_POST['submit']) { // if user didn't come from connexion page
	header('Location: ../connexion.php?source=noRoute');
}
else {
	if (!$userData) { // if no username is found
		header('Location: ../connexion.php?source=noMatch');
	}
	else {
		if (password_verify($inputedPass, $userData['password'])) {
			$_SESSION['loggedAs'] = $userData['username'];
			header('Location: ../index.php');
		}
		else { // if wrong password
			header('Location: ../connexion.php?source=wrongPass');
		}
	}
}